import { replaceUndefined } from './replace-undefined';

describe('replaceUndefined', () => {
  it('Should replace undefined to null', () => {
    const testObj: any = {
      a: false,
      b: null,
      c: undefined,
      d: 5,
      e: '',
      f: 'string',
    };

    expect(replaceUndefined(testObj)).toStrictEqual({
      a: false,
      b: null,
      c: null,
      d: 5,
      e: '',
      f: 'string',
    });
  });
});
