export const validators = {
  emailIsValid(email: string): boolean {
    return /^[^\s@]+@[^\s@]+\.[^\s@]+$/.test(email);
  },
};


export const hashById = <T extends { id: string }>(data: T[]) => {
  return data.reduce<{ [id: string]: T }>((acc, item) => {
    acc[item.id] = item;
    return acc;
  }, {});
};

export const capitalize = (text: string) => {
  return text.charAt(0).toUpperCase() + text.slice(1);
};
