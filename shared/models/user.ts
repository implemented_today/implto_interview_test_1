import { ModelBase } from './model-base';

export interface User extends ModelBase {
  id: string;
  roles: string[];
  email: string;
  displayName?: string;
  photoUrl?: string;
  lastSignInTime?: string;
  userInitialized?: boolean;
  approved?: boolean;
  tasks?: string[];
}
