export interface FunctionExecution {
  functionName: string;
  error?: string;
  startedAt?: string;
  finishedAt?: string;
}
