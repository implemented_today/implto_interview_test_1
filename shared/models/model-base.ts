import FieldValue = firestore.FieldValue;
import firebase from 'firebase/compat';
import firestore = firebase.firestore;

export interface ModelBase {
  createdAt: string | FieldValue;
  createdBy: string;
  updatedAt: string | FieldValue;
  updatedBy: string;
}
