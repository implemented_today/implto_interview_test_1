import { ModelBase } from './model-base';

export enum TaskComplexity {
  easy = 'easy',
  normal = 'normal',
  hard = 'hard',
  very_hard = 'very_hard'
}

export enum TaskMemberRole {
  developer = 'developer',
  qa = 'qa',
  other = 'other'
}

export interface Task extends ModelBase {
  id?: string;
  name: string;
  title: string;
  description: string;
  complexity?: TaskComplexity;
  member_role: TaskMemberRole;
}
