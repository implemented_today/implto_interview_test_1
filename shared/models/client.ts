import { ModelBase } from '@shared/models/model-base';

export enum ClientStatus {
    active = 'active',
    inactive = 'inactive'
}

export interface Client extends ModelBase {
    name: string;
    address: string;
    status: ClientStatus
}
