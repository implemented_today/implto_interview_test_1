export enum Role {
  admin = 'admin',
}

export interface UserRole {
  value: Role;
  label: string;
}

const projectRoles: UserRole[] = [
  {
    value: Role.admin,
    label: 'Admin',
  },
];

export default projectRoles;
