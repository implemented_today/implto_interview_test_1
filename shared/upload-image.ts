export interface UploadImageRequest {
  imageUrl: string;
  uploadPath: string;
  maxWidth?: number;
  maxHeight?: number;
}

export interface UploadImageResponse {
  url: string;
}
