import { pick } from './pick';

export type DataRecord<T> = T & {
  getChangedFields(extra?: (keyof T)[]): Partial<T>,
};

export function isDataRecord<T extends any>(item: T): item is DataRecord<T> {
  return (item as DataRecord<T>).getChangedFields !== undefined;
}

// eslint-disable-next-line @typescript-eslint/ban-types
export function createDataRecord<T extends object>(object: T): DataRecord<T> {
  const changedFields = new Set();

  return new Proxy(object, {

    get: (target: any, propertyKey) => {
      if (propertyKey === 'getChangedFields') {
        return (extra: (keyof T)[] = []) => pick(object, [...changedFields, ...extra] as any);
      }
      return target[propertyKey];
    },
    set: (target: any, propertyKey, value: any) => {
      if (target[propertyKey] !== value) {
        changedFields.add(propertyKey);
      }

      target[propertyKey] = value;
      return true;
    },
  }) as unknown as DataRecord<T>;
}
