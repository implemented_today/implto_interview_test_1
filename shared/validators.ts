import { URL } from 'url';

export function emailIsValid(email: string): boolean {
  return /^[^\s@]+@[^\s@]+\.[^\s@]+$/.test(email);
}

export function urlIsValid(url: string): boolean {
  let urlObj;

  try {
    urlObj = new URL(url);
  } catch (err) {
    return false;
  }

  return urlObj.protocol === 'http:' || urlObj.protocol === 'https:';
}
