type ValidationErrorCode = 'missing-property' | 'not-valid-property';

export class ValidationError extends Error {
  readonly fieldName: string;
  readonly code: ValidationErrorCode;

  constructor(code: ValidationErrorCode, fieldName?: string, customMessage?: string) {
    const message = customMessage || ValidationError.prepareErrorText(code, fieldName);
    super(message);
    this.fieldName = fieldName;
    this.code = code;
  }

  toJSON() {
    const {message, code, fieldName} = this;
    return {
      message,
      code,
      fieldName,
    };
  }

  private static prepareErrorText(code: ValidationErrorCode, fieldName?: string) {
    let message = `Not valid property${fieldName ? ' : ' + fieldName : ''}`;
    switch (code) {
      case 'missing-property':
        message = `Missing property${fieldName ? ' : ' + fieldName : ''}`;
        break;
    }
    return message;
  }
}
