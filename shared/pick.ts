export function pick<T, P extends keyof T>(obj: T, fields: readonly P[]): Pick<T, P> {
  const out: any = {};

  fields.forEach((key) => {
    if (obj[key] !== undefined) {
      out[key] = obj[key];
    }
  });

  return out;
}
