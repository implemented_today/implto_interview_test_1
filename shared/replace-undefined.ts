/**
 * Return object replacing fields with "undefined" value to null
 *
 * Why? Firestore doesn't have an `undefined` type.
 * We couldn't just ignore fields with undefined because
 * there are cases where we update records and want to reset fields to "empty value"
 */
export function replaceUndefined<T>(d: T): Partial<T> {
  return Object.keys(d).reduce((acc, field) => {
    const value = (d as any)[field];
    (acc as any)[field] = value === undefined ? null : value;

    return acc;
  }, {});
}
