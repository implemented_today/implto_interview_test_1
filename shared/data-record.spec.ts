import { createDataRecord } from './data-record';

describe('Data Record', () => {
  it('Should return changed properties', () => {
    const object = { a: 1, b: 2, c: 3 };
    const record = createDataRecord(object);

    record.b = 3;
    record.c = 4;

    expect(record.getChangedFields()).toStrictEqual({
      b: 3,
      c: 4,
    });
  });
});
