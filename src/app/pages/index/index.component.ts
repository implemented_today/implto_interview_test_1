import { Component } from '@angular/core';

@Component({
  selector: 'index-pages',
  template: `<router-outlet></router-outlet>`,
})
export class IndexComponent {
}
