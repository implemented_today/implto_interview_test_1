import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { TasksListComponent } from './tasks-list.component';
import { Routes, RouterModule } from '@angular/router';
import { MatExpansionModule } from '@angular/material/expansion';
import { NbCardModule } from '@nebular/theme';
import { QuillContentRendererModule } from '../../../components/quill-content-renderer/quill-content-renderer.module';

export const routes: Routes = [
  {
    path: '',
    component: TasksListComponent,
  },
];

@NgModule({
  declarations: [TasksListComponent],
  imports: [
    CommonModule,
    RouterModule.forChild(routes),
    MatExpansionModule,
    NbCardModule,
    QuillContentRendererModule
  ],
})
export class TasksListModule {}
