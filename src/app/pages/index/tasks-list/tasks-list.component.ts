import { Component } from '@angular/core';
import { AngularFirestore } from '@angular/fire/compat/firestore';
import { TASKS_COLLECTION } from '@shared/firestore-collections-names';
import { defer, Observable, of } from 'rxjs';
import { UserService } from '../../../auth/user.service';
import { map, switchMap, tap } from 'rxjs/operators';
import { Task } from '@shared/models/task';

@Component({
  selector: 'tasks-list',
  templateUrl: './tasks-list.component.html',
  providers: [],
})
export class TasksListComponent {
  tasks: Task[] = null;

  tasks$: Observable<Task[]> = defer(() =>
    this.userService.dbUser$.pipe(
      switchMap((user) => {
        if (!user.tasks?.length) {
          return of([]);
        }
        return this.firestore.collection<Task>(TASKS_COLLECTION, (ref) => {
          return ref.where('__name__', 'in', user.tasks);
        }).valueChanges({idField: 'id'}).pipe(
          map((tasks) => tasks.sort((a, b) => {
            return user.tasks.indexOf(a.id) - user.tasks.indexOf(b.id)
          }))
        )
      }),
      tap((tasks) => {
        this.tasks = tasks;
      })
    )
  );

  constructor(
    private firestore: AngularFirestore,
    private userService: UserService,
  ) {}
}
