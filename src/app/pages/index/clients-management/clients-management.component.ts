import { Component } from '@angular/core';
import { ColDef } from '@ag-grid-enterprise/all-modules';
import { SimpleCrudOptions } from '../../../grid-features/simple-crud/simple-crud.component';
import { NotificationService } from '../../../services/notification.service';
import { ListItemsPickerColumnSimple } from '../../../grid-features/list-items-picker-comumn-builders/list-items-picker-column-simple';
import { validators } from '@shared/utils';
import { dateComparatorFirestoreTimestamp, dateValueFormatterFirestoreTimestamp } from '../../../utils/utils';
import { AngularFirestore, AngularFirestoreCollection } from '@angular/fire/compat/firestore';
import { UserService } from '../../../auth/user.service';
import { Client, ClientStatus } from '@shared/models/client';

@Component({
  selector: 'clients-management',
  template: `
    <app-simple-crud *ngIf="collectionRef" [collectionRef]="collectionRef" [colDefs]="colDefs"
                     uniqName="clients-management" [options]="simpleCrudOptions"></app-simple-crud>
  `,
})
export class ClientsManagementComponent {
  collectionRef: AngularFirestoreCollection = this.firestore.collection<Client>(this.userService.getCollectionPath(`clients`));
  colDefs: ColDef[] = [
    {
      field: 'email',
      headerName: 'Email',
      filter: 'agTextColumnFilter',
      floatingFilter: true,
      editable: false,
    },
    {
      field: 'displayName',
      headerName: 'Display name',
      filter: 'agTextColumnFilter',
      floatingFilter: true,
      editable: true,
    },
    {
      field: 'createdAt',
      headerName: 'Created',
      filter: false,
      floatingFilter: false,
      valueFormatter: dateValueFormatterFirestoreTimestamp,
      comparator: dateComparatorFirestoreTimestamp,
    },
    {
      field: 'status',
      headerName: 'Status',
      ...new ListItemsPickerColumnSimple({multi: false})
        .setOptions([
          {
            value: ClientStatus.active,
            label: 'Active',
          },
          {
            value: ClientStatus.inactive,
            label: 'Inactive',
          },
        ])
        .build(),
    },
  ];

  constructor(
    private firestore: AngularFirestore,
    private notificationService: NotificationService,
    protected userService: UserService,
  ) {
  }

  get simpleCrudOptions(): SimpleCrudOptions<Client> {
    return {
      itemKey: 'email',
      useKeyAsId: true,
      beforeAdd: async (email): Promise<boolean> => {
        if (!validators.emailIsValid(email as string)) {
          this.notificationService.showError(`Email not valid`);
          return false;
        }
        if ((await this.collectionRef.doc(email as string).get().toPromise()).exists) {
          this.notificationService.showError(`Client with this email already exists`);
          return false;
        }
        return true;
      },
      baseProperties: {
        status: ClientStatus.active,
      },
    };
  }
}
