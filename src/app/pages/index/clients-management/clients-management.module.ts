import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { ClientsManagementComponent } from './clients-management.component';
import { Routes, RouterModule } from '@angular/router';
import { SimpleCrudModule } from '../../../grid-features/simple-crud/simple-crud.module';

export const routes: Routes = [
  {
    path: '',
    component: ClientsManagementComponent,
  },
];

@NgModule({
  declarations: [ClientsManagementComponent],
  imports: [
    CommonModule,
    RouterModule.forChild(routes),
    SimpleCrudModule,
  ],
})
export class ClientsManagementModule {}
