import { RouterModule, Routes } from '@angular/router';
import { NgModule } from '@angular/core';

import { IndexComponent } from './index.component';
import { ClientsManagementComponent } from './clients-management/clients-management.component';
import { TasksListComponent } from './tasks-list/tasks-list.component';

const routes: Routes = [{
  path: '',
  component: IndexComponent,
  children: [
    {
      path: 'clients',
      component: ClientsManagementComponent,
    },
    {
      path: 'tasks',
      component: TasksListComponent,
    },
    {path: '**', redirectTo: '/tasks'},
  ],
}];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class IndexRoutingModule {
}
