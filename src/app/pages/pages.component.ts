import { Component, OnDestroy, OnInit } from '@angular/core';

import { UserService } from '../auth/user.service';
import { NbMenuItem } from '@nebular/theme';
import { Subject } from 'rxjs';

@Component({
  selector: 'ngx-pages',
  template: `
    <ngx-one-column-layout>
      <nb-menu [items]="menu"></nb-menu>
      <router-outlet></router-outlet>
    </ngx-one-column-layout>
  `,
})
export class PagesComponent implements OnInit, OnDestroy {
  public menu: NbMenuItem[] = [];
  private destroy$: Subject<void> = new Subject<void>();


  constructor(
    private userService: UserService,
  ) {
  }

  ngOnInit(): void {
    this.prepareMenuLinks();
  }

  private prepareMenuLinks() {
    const MENU_ITEMS: (NbMenuItem & { role?: string }) [] = [
      {
        title: 'Tasks',
        icon: 'checkmark-square-outline',
        link: `/tasks`,
      },
      {
        title: 'Clients',
        icon: 'people',
        link: `/clients`,
      }
    ];

    this.updateMenu(MENU_ITEMS.filter((item) => !item.role || this.userService.hasRole(item.role)));
  }

  private updateMenu(newMenu: NbMenuItem[]) {
    const updateItemsLinks = (items: NbMenuItem[], newItems: NbMenuItem[]) => {
      if (items.length !== newItems.length) {
        return false;
      }
      for (const [i, item] of items.entries()) {
        if (newItems[i]?.title !== item.title) {
          return false;
        }
        if (item.children) {
          updateItemsLinks(item.children, newItems[i].children as NbMenuItem[]);
        } else {
          item.link = newItems[i].link;
        }
      }
      return true;
    };

    if (!this.menu || !updateItemsLinks(this.menu, newMenu)) {
      this.menu = newMenu;
    }
  }

  ngOnDestroy() {
    this.destroy$.next();
    this.destroy$.complete();
  }
}
