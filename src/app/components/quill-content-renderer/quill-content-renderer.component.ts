import { Component, Input } from '@angular/core';

@Component({
  selector: 'implto-quill-content-renderer',
  template: `
    <div [class]="themeClass">
      <div class="ql-editor" [innerHTML]="html"></div>
    </div>
  `,
})
export class QuillContentRendererComponent {
  @Input() theme = 'snow';

  @Input() html: string;

  get themeClass() {
    return {
      [`ql-${this.theme}`]: true,
    }
  }
}
