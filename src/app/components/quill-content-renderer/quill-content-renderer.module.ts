import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { QuillContentRendererComponent } from './quill-content-renderer.component';

@NgModule({
  declarations: [QuillContentRendererComponent],
  imports: [
    CommonModule,
  ],
  exports: [
    QuillContentRendererComponent,
  ],
})
export class QuillContentRendererModule {}
