import { Directive, Input, TemplateRef, ViewContainerRef } from '@angular/core';

interface LetContext<T> {
  cdkLet: T;
}

@Directive({
  // tslint:disable-next-line:directive-selector
  selector: '[cdkLet]',
})
export class CdkLetDirective<T> {
  // tslint:disable-next-line:variable-name
  private _context: LetContext<T> = { cdkLet: null };

  // tslint:disable-next-line:variable-name
  constructor(_viewContainer: ViewContainerRef, _templateRef: TemplateRef<LetContext<T>>) {
    _viewContainer.createEmbeddedView(_templateRef, this._context);
  }

  @Input()
  set cdkLet(value: T) {
    this._context.cdkLet = value;
  }
}
