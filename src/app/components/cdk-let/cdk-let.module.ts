import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { CdkLetDirective } from './cdk-let.directive';

@NgModule({
  declarations: [CdkLetDirective],
  imports: [
    CommonModule,
  ],
  exports: [CdkLetDirective],
})
export class CdkLetModule {}
