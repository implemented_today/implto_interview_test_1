import { FormGroup, FormControl, ValidationErrors, AbstractControl, ValidatorFn, FormArray } from '@angular/forms';
import { Observable } from 'rxjs';
import { startWith, shareReplay } from 'rxjs/operators';

type ValidatorDef = ValidatorFn | ValidatorFn[];
type ValueDef<T> = { value: T, disabled: boolean } | T;

export type AsFormControlConfig<T> = {
  [P in keyof T]: [ValueDef<T[P]>] | [ValueDef<T[P]>, ValidatorDef];
};

export function markFormGroupTouched(form: AbstractControl) {
  traverseForm(form, (control) => {
    if (!control.disabled) {
      control.markAsTouched();
      control.markAsDirty();
      control.updateValueAndValidity({emitEvent: false});
    }
  });
}

function traverseForm(form: AbstractControl, callback: (control: FormControl, path: string) => void) {
  // tslint:disable-next-line:variable-name
  const _traverseForm = (control: AbstractControl, path: (string | number)[] = []) => {
    if (control instanceof FormGroup) {
      Object.keys(control.controls)
        .forEach((controlName: string) => {
          _traverseForm(control.controls[controlName], path.concat([controlName]));
        });
    } else if (control instanceof FormArray) {
      control.controls.forEach((child, index) => {
        _traverseForm(child, path.concat([index]));
      });
    } else {
      callback(control as FormControl, path.join('.'));
    }
  };
  _traverseForm(form);
}

export function getFormValidationErrors(form: FormGroup) {
  const result: {
    path: string,
    error: string,
  }[] = [];

  traverseForm(form, (control, path) => {
    const controlErrors: ValidationErrors = control.errors;
    if (controlErrors) {
      Object.keys(controlErrors).forEach(keyError => {
        result.push({
          path,
          error: keyError,
        });
      });
    }
  });

  return result;
}

export function scrollToInvalid() {
  const invalidEl = document.querySelector('.ng-invalid:not(form):not([formgroupname])');

  if (invalidEl) {
    invalidEl.scrollIntoView({
      behavior: 'smooth',
    });
  }
}

type BindingExpr = string | { deps: string[], link: (...deps: Observable<any>[]) => Observable<boolean> };

/**
 * Since using [disabled] binding together with ReactiveForms is no allowed by Angular
 * this function create a binding between one form field to disabled property of another
 * and incapsulate all heavy lifting inside
 *
 * There two ways of using:
 *
 * 1. Simple usage if you have one-to-one binding
 * ```
 * createFormDisabledBinding(this.form, 'documentExpireDate', 'documentEndless')
 * ```
 *
 * This means when documentEndless field will be true, documentExpireDate will become disabled
 *
 * 2. If calculation of disabled wield relays on more then one property from form
 * ```
 * createFormDisabledBinding(this.form, 'documentExpireDate', {
 *   deps: ['documentEndless', 'needDocument'],
 *   link: (documentEndless$, needDocument$) => Observable<boolean>
 * })
 *
 * Here value of disabled state computed from two properties from form
 * ```
 */
export function createFormDisabledBinding(form: FormGroup, controlName: string, binding: BindingExpr) {
  // tslint:disable-next-line:variable-name
  const getValueObservableByName = (_controlName: string) => {
    return getValueObservable(form.get(_controlName));
  };

  const control = form.get(controlName);

  const subscribeToState = (obs: Observable<boolean>) => {
    obs.subscribe((state) => state ? control.disable() : control.enable());
  };

  if (typeof binding === 'string') {
    subscribeToState(getValueObservableByName(binding));
  } else {
    const deps: Observable<any>[] = binding.deps.map(getValueObservableByName);
    subscribeToState(binding.link(...deps));
  }
}

/**
 * We need this helper because RX Forms sucks, and not enough 'reactive'
 * means valueChanges observable is triggered only when data was changed,
 * and cannot be used to observe existing value
 *
 * This helper create a cold observable similar to BehaviourSubject,
 * which always has a value whenever you subscribe on it
 */
export function getValueObservable(control: AbstractControl) {
  return control.valueChanges.pipe(startWith(control.value), shareReplay());
}
