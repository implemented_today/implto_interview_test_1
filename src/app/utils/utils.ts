import {ValueFormatterParams} from '@ag-grid-community/core/dist/cjs/entities/colDef';
import firebase from 'firebase/compat';
import Timestamp = firebase.firestore.Timestamp;


export function dateComparator(a: string, b: string) {
  return (new Date(a)).getTime() - (new Date(b)).getTime();
}

export function dateComparatorFirestoreTimestamp(a: Timestamp, b: Timestamp) {
  return (new Date(a.toDate())).getTime() - (new Date(b.toDate())).getTime();
}

export const dateValueFormatter = (params: ValueFormatterParams) => params.value ? (new Date(params.value)).toLocaleString() : '';

export const dateValueFormatterFirestoreTimestamp = (params: ValueFormatterParams) =>
  params.value ? (new Date(params.value.toDate())).toLocaleString() : '';
