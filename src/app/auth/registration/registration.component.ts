import { ChangeDetectorRef, Component, Inject } from '@angular/core';
import { NbRegisterComponent, NB_AUTH_OPTIONS, NbAuthService } from '@nebular/auth';
import { Router } from '@angular/router';
import {AuthService} from '../auth.service';
import {NotificationService} from '../../services/notification.service';

@Component({
  templateUrl: './registration.component.html',
  styleUrls: ['./registration.component.scss'],
})
export class RegistrationComponent extends NbRegisterComponent {
  constructor(
    protected service: NbAuthService,
    @Inject(NB_AUTH_OPTIONS) protected options = {},
    protected cd: ChangeDetectorRef,
    protected router: Router,
    private authService: AuthService,
    private notificationService: NotificationService,
  ) {
    super(service, options, cd, router);
  }

  public async register() {
    try {
      await this.authService.registerWithEmail(this.user.email, this.user.password);
    }
    catch (e) {
      this.notificationService.showError(e.message);
    }
  }
}
