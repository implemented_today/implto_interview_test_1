import { Injectable } from '@angular/core';
import { CanActivate, Router } from '@angular/router';

import { UserService } from './user.service';
import { take } from 'rxjs/operators';
import { NotificationService } from '../services/notification.service';
import { AngularFireAuth } from '@angular/fire/compat/auth';

@Injectable({providedIn: 'root'})
export class AuthGuard implements CanActivate {
  constructor(
    private afAuth: AngularFireAuth,
    private userService: UserService,
    private errorService: NotificationService,
    private router: Router,
  ) {}

  async canActivate() {
    const currentUser = await this.afAuth.user.pipe(take(1)).toPromise();

    if (!currentUser) {
      return this.router.createUrlTree(['auth', 'login']);
    }
    if (!currentUser.emailVerified) {
      return this.showErrorAndGoToLogin(`Please, verify your email address to access to the system: ${currentUser.email}`);
    }

    if (await this.userService.checkAccess(currentUser)) {
      return true;
    }

    return this.showErrorAndGoToLogin();
  }

  private showErrorAndGoToLogin(error?: string) {
    this.errorService.showError(
      error || 'You don\'t have permissions. Please contact system administrator',
    );

    return this.router.createUrlTree(['auth', 'login']);
  }
}
