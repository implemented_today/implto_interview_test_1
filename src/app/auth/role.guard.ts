import { Injectable } from '@angular/core';
import { CanActivate, ActivatedRouteSnapshot, Router } from '@angular/router';
import { UserService } from './user.service';
import { NotificationService } from '../services/notification.service';

@Injectable({ providedIn: 'root' })
export class RoleGuard implements CanActivate {
  constructor(
    private userService: UserService,
    private errorService: NotificationService,
    private router: Router,
  ) {}

  canActivate(route: ActivatedRouteSnapshot) {
    const roles = route.data.roles as string[];
    if (roles.some((role) => this.userService.hasRole(role))) {
      return true;
    }
    this.errorService.showError('You don\'t have permissions to access to this page');
    return this.router.createUrlTree(['']);
  }
}

export function canActivateForRoles(roles: string[]) {
  return { data: { roles }, canActivate: [RoleGuard] };
}
