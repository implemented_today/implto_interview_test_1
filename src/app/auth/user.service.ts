import { BehaviorSubject, defer, Observable } from 'rxjs';
import { Injectable } from '@angular/core';
import { USERS_COLLECTION } from '@shared/firestore-collections-names';
import { User } from '@shared/models/user';
import { map, shareReplay, switchMap, take, tap } from 'rxjs/operators';
import { AngularFirestore } from '@angular/fire/compat/firestore';
import { AngularFireFunctions } from '@angular/fire/compat/functions';
import firebase from 'firebase/compat';
import UserInfo = firebase.UserInfo;

@Injectable({providedIn: 'root'})
export class UserService {
  private initializeUserFunction = this.functions.httpsCallable('initializeUser');

  dbUser$: Observable<User> = defer(() => {
    if (!this.user) {
      throw new Error('Firestore user not found. Use user$ after auth guard');
    }
    return this.firestore.collection<User>(USERS_COLLECTION).doc(this.user.email).snapshotChanges();
  }).pipe(
    switchMap(async (action): Promise<User> => {
      if (!action.payload.exists) {
        return this.initializeUser();
      }
      const user = action.payload.data() as User;
      if (!user.userInitialized) {
        return this.initializeUser();
      }
      return user;
    }),
    map((user) => {
      if (user) {
        user.photoUrl = this.user.photoURL;
      }
      return user;
    }),
    tap((user) => {
      this.dbUser = user;
    }),
    shareReplay(1),
  );
  dbUser: User;

  me$ = new BehaviorSubject<UserInfo>(null);

  constructor(
    private firestore: AngularFirestore,
    private functions: AngularFireFunctions,
  ) {
  }

  get user() {
    return this.me$.value;
  }

  get me() {
    return this.me$.value;
  }

  public async checkAccess(firebaseUser: UserInfo) {
    this.setUser(firebaseUser);

    await this.dbUser$.pipe(take(1)).toPromise();

    return this.dbUser?.approved;
  }

  public hasRole(role: string) {
    return this.dbUser?.roles?.includes(role);
  }

  public getCollectionPath(collectionName: string): string {
    return `data/${this.dbUser.email}/${collectionName}`;
  }

  private setUser(firebaseUser: UserInfo) {
    this.me$.next(firebaseUser);
  }

  /*
   * Function calls backend function and set default roles for user depends on email domain
   * and create user in DB if it doesn't exist
   */
  private async initializeUser(): Promise<User> {
    try {
      await this.initializeUserFunction({}).toPromise();
    }
    catch (e) {
      console.error('initializeUser error', e);
    }
    return (await this.firestore.collection<User>(USERS_COLLECTION).doc(this.user.email).get().toPromise()).data() as User;
  }
}
