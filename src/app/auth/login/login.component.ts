import { Component, OnDestroy, ChangeDetectorRef, Inject } from '@angular/core';
import { NbLoginComponent, NB_AUTH_OPTIONS, NbAuthService } from '@nebular/auth';
import { Subject } from 'rxjs';
import { Router } from '@angular/router';
import { AuthService } from '../auth.service';
import { NotificationService } from '../../services/notification.service';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.scss'],
})
export class LoginComponent extends NbLoginComponent implements OnDestroy {
  private destroy$ = new Subject();

  constructor(
    protected service: NbAuthService,
    @Inject(NB_AUTH_OPTIONS) protected options = {},
    protected cd: ChangeDetectorRef,
    protected router: Router,
    private authService: AuthService,
    private notificationService: NotificationService,
  ) {
    super(service, options, cd, router);
  }

  public ngOnDestroy() {
    this.destroy$.next();
  }

  public async login() {
    try {
      await this.authService.signinWithEmail(this.user.email, this.user.password);
    }
    catch (e) {
      this.notificationService.showError(e.message);
    }
  }

  async signinGoogle() {
    await this.authService.signinGoogle();
  }
}
