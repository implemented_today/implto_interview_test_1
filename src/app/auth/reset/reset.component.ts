import { Component, ChangeDetectorRef, Inject } from '@angular/core';
import { NbRequestPasswordComponent, NB_AUTH_OPTIONS, NbAuthService } from '@nebular/auth';
import { Router } from '@angular/router';
import {AuthService} from '../auth.service';
import {NotificationService} from '../../services/notification.service';

@Component({
  selector: 'app-reset',
  templateUrl: './reset.component.html',
  styleUrls: ['./reset.component.scss'],
})
export class ResetComponent extends NbRequestPasswordComponent {
  constructor(
    protected service: NbAuthService,
    @Inject(NB_AUTH_OPTIONS) protected options = {},
    protected cd: ChangeDetectorRef,
    protected router: Router,
    private authService: AuthService,
    private notificationService: NotificationService,
  ) {
    super(service, options, cd, router);
  }

  public async requestPass() {
    try {
      await this.authService.sendPasswordResetEmail(this.user.email);
      this.notificationService.showMessage('We sent instructions about password reset to your email');
      await this.router.navigate(['/']);
    }
    catch (e) {
      this.notificationService.showError(e.message);
    }
  }
}
