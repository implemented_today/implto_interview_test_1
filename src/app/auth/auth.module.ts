import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { LoginComponent } from './login/login.component';
import { RegistrationComponent } from './registration/registration.component';
import { ResetComponent } from './reset/reset.component';
import {
  NbAuthComponent,
  NbLogoutComponent,
} from '@nebular/auth';
import { Routes, RouterModule } from '@angular/router';
import { NbAlertModule, NbInputModule, NbCheckboxModule, NbButtonModule } from '@nebular/theme';
import { FormsModule } from '@angular/forms';
import { AuthGuard } from './auth.guard';

export const routes: Routes = [
  {
    path: '',
    component: NbAuthComponent,
    children: [
      {
        path: 'login',
        component: LoginComponent,
      },
      {
        path: 'register',
        component: RegistrationComponent,
      },
      {
        path: 'logout',
        component: NbLogoutComponent,
      },
      {
        path: 'request-password',
        component: ResetComponent,
      },
      {path: '', redirectTo: 'login', pathMatch: 'full'},
    ],
  },
];

@NgModule({
  declarations: [
    LoginComponent,
    RegistrationComponent,
    ResetComponent,
  ],
  imports: [
    RouterModule.forChild(routes),
    CommonModule,
    NbAlertModule,
    FormsModule,
    NbInputModule,
    NbCheckboxModule,
    NbButtonModule,
  ],
  providers: [AuthGuard],
})
export class AuthModule {
}
