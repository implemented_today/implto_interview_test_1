import { Injectable } from '@angular/core';
import { Router } from '@angular/router';
import { AngularFireAuth } from '@angular/fire/compat/auth';
import firebase from 'firebase/compat/app';
import UserCredential = firebase.auth.UserCredential;
import auth = firebase.auth;

@Injectable({providedIn: 'root'})
export class AuthService {
  constructor(
    protected router: Router,
    private afa: AngularFireAuth,
  ) {
  }

  public async signinGoogle(): Promise<UserCredential> {
    const result = await this.afa.signInWithPopup(new auth.GoogleAuthProvider());
    await this.afterSignIn(result);
    return result;
  }

  public async signinWithEmail(email: string, password: string): Promise<UserCredential> {
    const result = await this.afa.signInWithEmailAndPassword(email, password);
    await this.afterSignIn(result);
    return result;
  }

  public async registerWithEmail(email: string, password: string): Promise<UserCredential> {
    const result = await this.afa.createUserWithEmailAndPassword(email, password);
    await result.user.sendEmailVerification();
    await this.afterSignIn(result);
    return result;
  }

  public async sendPasswordResetEmail(email: string) {
    return await this.afa.sendPasswordResetEmail(email);
  }

  private async afterSignIn(_result: UserCredential) {
    await this.router.navigate(['/']);
  }
}
