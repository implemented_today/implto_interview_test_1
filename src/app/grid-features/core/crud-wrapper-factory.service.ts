import { Injectable } from '@angular/core';
import { AngularFirestore, AngularFirestoreCollection } from '@angular/fire/compat/firestore';
import firebase from 'firebase/compat/app';
import firestore = firebase.firestore;

export interface UpdateRowParams<T> {
  oldValue: any;
  value: any;
  field: keyof T;
  row: T;
}

@Injectable({
  providedIn: 'root',
})
export class CrudWrapperFactory {
  constructor(
    private afs: AngularFirestore,
  ) { }

  create(collectionPath: string) {
    const collectionRef = this.afs.collection(collectionPath);
    return new CrudWrapper(collectionRef);
  }
}

export class CrudWrapper<T extends { [field: string]: any }> {
  constructor(
    public collectionRef: AngularFirestoreCollection,
  ) {}

  public async deleteDoc(id: string): Promise<void> {
    return await this.collectionRef.doc(id).delete();
  }

  public async updateDoc({row, field, value}: UpdateRowParams<T>): Promise<void> {
    if (!row.id) {
      return;
    }
    return await this.collectionRef.doc(row.id).update({
      [field]: value,
      updatedAt: firestore.FieldValue.serverTimestamp(),
      updatedBy: firebase.auth().currentUser.email,
    });
  }

  public async getCollection(): Promise<any> {
    return this.collectionRef.get();
  }

  public async addDoc(data: T, docCustomKey: string = null) {
    const dataToSave = {
      ...data,
      createdBy: firebase.auth().currentUser.email,
      updatedBy: firebase.auth().currentUser.email,

      createdAt: firebase.firestore.FieldValue.serverTimestamp(),
      updatedAt: firebase.firestore.FieldValue.serverTimestamp(),
    };

    if (docCustomKey) {
      await this.collectionRef.doc(docCustomKey).set(dataToSave, {merge: true});
    }
    else {
      await this.collectionRef.add(dataToSave);
    }
  }

  public async updateFullDoc(data: T): Promise<void> {
    const id = data.id;
    data = JSON.parse(JSON.stringify(data));
    delete data.id;
    const dataToSave = {
      ...data,
      updatedBy: firebase.auth().currentUser.email,
      updatedAt: firebase.firestore.FieldValue.serverTimestamp(),
    };
    await this.collectionRef.doc(id).set(dataToSave, {merge: true});
  }
}

