import { ICellRendererComp, ICellRendererParams } from '@ag-grid-enterprise/all-modules';

export interface ToolbarAction {
  onClick: () => void;
  icon: string;
  title?: string;
  type?: string;
  disable?: boolean;
}

export interface BtnToolbarRendererParams {
  actions: ToolbarAction[] | ((data: any) => ToolbarAction[]);
}

export class BtnToolbarRenderer implements ICellRendererComp {
  private eGui: HTMLDivElement;
  private actions: ToolbarAction[];

  public init(params: ICellRendererParams & BtnToolbarRendererParams) {
    this.actions =  typeof params.actions === 'function' ? params.actions(params.data) : params.actions;

    this.eGui = document.createElement('div');
    this.eGui.style.cssText =
      'display: flex;' +
      'align-items: center;' +
      'height: 100%;';

    this.eGui.innerHTML = this.actions.map((action) => {
      return `<button title="${action.title}" disabled="${action.disable}" class="p-button-${action.type || 'primary'} p-button-text p-ripple p-button p-component p-button-icon-only"><i class="pi pi-${action.icon}"></i></button>`;
    }).join('\n');

    this.eGui.addEventListener('click', this.btnClickedHandler);
  }

  public getGui() {
    return this.eGui;
  }

  public refresh(): boolean {
    return false;
  }

  public destroy() {
    this.eGui.removeEventListener('click', this.btnClickedHandler);
  }

  private btnClickedHandler = (event: MouseEvent) => {
    const target = (event.target as HTMLElement);

    const button = target.matches('button') ? target : target.closest('button');
    const index = Array.from(this.eGui.children).indexOf(button);
    this.actions[index].onClick();
  }
}
