import { ListItemsPickerColumnSimple } from './list-items-picker-column-simple';
import { AngularFirestoreCollection } from '@angular/fire/compat/firestore';
import { Option } from '../cell-editors/list-items-picker-cell-editor/list-items-picker-cell-editor.component';

export class ListItemsPickerColumnFirebaseOptions extends ListItemsPickerColumnSimple {
  public async prepareOptions(optionsCollection: AngularFirestoreCollection) {
    const optionsSnapshot = await optionsCollection.get().toPromise();

    const docs: any[] = optionsSnapshot.docs.map((doc) => ({
      id: doc.id,
      ...doc.data(),
    }));
    const options = docs.map<Option>((item) => ({label: this.renderLabel(item), value: item.id}));

    return super.setOptions(options);
  }
}
