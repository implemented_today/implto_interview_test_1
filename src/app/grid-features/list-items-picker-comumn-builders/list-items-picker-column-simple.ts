import {ColDef, ValueFormatterParams} from '@ag-grid-enterprise/all-modules';
import {
  ListItemsPickerCellEditorComponent,
  ListItemsPickerCellEditorParams,
  Option
} from '../cell-editors/list-items-picker-cell-editor/list-items-picker-cell-editor.component';

export interface ListItemsPickerOptions {
  labelKey?: string;
  labelRenderer?<T extends { [field: string]: any }>(rowData: T): string;
}

export class ListItemsPickerColumnSimple {
  private options: Option[];
  private readonly cellEditorParams: Partial<Omit<ListItemsPickerCellEditorParams, 'options'>>;
  private readonly listItemsPickerOptions: ListItemsPickerOptions = {
    labelKey: 'name',
  };

  constructor(cellEditorParams: Partial<Omit<ListItemsPickerCellEditorParams, 'options'>> = {},
              listItemsPickerOptions: ListItemsPickerOptions = {}) {
    this.cellEditorParams = cellEditorParams;
    Object.assign(this.listItemsPickerOptions, listItemsPickerOptions);
  }

  public build(): ColDef {
    return this.getColDef();
  }

  public setOptions(options: Option[]) {
    this.options = options;
    return this;
  }

  private getLabelByEntityId(options: Option[], id: any): string {
    const el = options.find((option) => option.value === id);
    return el ? el.label : '';
  }

  private getColDef(): ColDef {
    return {
      editable: true,
      cellEditorFramework: ListItemsPickerCellEditorComponent,
      valueFormatter: (params) => {
        if (this.cellEditorParams.multi) {
          const ids = params.value && Array.isArray(params.value) ? params.value as string[] : [];
          return ids.map((id) => this.getLabelByEntityId(this.options, id)).filter((el) => !!el).join(', ');
        } else {
          const id = params.value === undefined ? '' : params.value;
          return this.getLabelByEntityId(this.options, id);
        }
      },
      cellEditorParams: {
        options: this.options,
        ...this.cellEditorParams,
      } as ListItemsPickerCellEditorParams,
      filterParams: {
        valueFormatter: (params: ValueFormatterParams) => {
          const id = params.value === undefined ? '' : params.value;
          return this.getLabelByEntityId(this.options, id);
        },
      },
    };
  }

  protected renderLabel(rowData: { [field: string]: any }) {
    if (this.listItemsPickerOptions.labelRenderer) {
      return this.listItemsPickerOptions.labelRenderer(rowData);
    }
    else {
      return rowData[this.listItemsPickerOptions.labelKey];
    }
  }
}
