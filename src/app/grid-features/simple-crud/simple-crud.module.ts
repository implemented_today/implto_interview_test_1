import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { SimpleCrudComponent } from './simple-crud.component';
import { NbCardModule } from '@nebular/theme';
import { AgGridModule } from '@ag-grid-community/angular';
import { ButtonModule } from 'primeng/button';
import { InputTextModule } from 'primeng/inputtext';
import { RippleModule } from 'primeng/ripple';



@NgModule({
  declarations: [SimpleCrudComponent],
  imports: [
    CommonModule,
    NbCardModule,
    AgGridModule,
    ButtonModule,
    InputTextModule,
    RippleModule,
  ],
  exports: [
    SimpleCrudComponent,
  ],
})
export class SimpleCrudModule { }
