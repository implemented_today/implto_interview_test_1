import { Component, Input, OnDestroy } from '@angular/core';
import { AllModules, ColDef, GridReadyEvent } from '@ag-grid-enterprise/all-modules';
import { BtnToolbarRenderer, BtnToolbarRendererParams, ToolbarAction } from '../renderers/btn-toolbar-renderer';
import { CellValueChangedEvent, GridApi, GridOptions } from '@ag-grid-community/core';
import { defer, Subscription } from 'rxjs';
import { map } from 'rxjs/operators';
import { syncStateToLocalStorage } from '../utils/sync-state-to-local-storage';
import { CrudWrapper, UpdateRowParams } from '../core/crud-wrapper-factory.service';
import { AngularFirestoreCollection } from '@angular/fire/compat/firestore';

export interface SimpleCrudOptions<T extends { [field: string]: any }> {
  showAddNew?: boolean;
  showAddNewAsTableRow?: 'top' | 'bottom';
  showFilter?: boolean;
  itemKey?: string;
  useKeyAsId?: boolean;
  baseProperties?: {
    [key in keyof(T)]?: any
  };
  additionalActions?(data: T, crudWrapper: CrudWrapper<T>): [ToolbarAction?];
  keyGenerator?(itemKeyValue: string): string;
  dataTransform?(data: T): T;
  beforeAdd?(data: string | T): Promise<boolean>;
  beforeUpdate?(params: UpdateRowParams<T>): Promise<boolean>;
  beforeDelete?(data: T): Promise<boolean>;
}

@Component({
  selector: 'app-simple-crud',
  template: `
    <nb-card class="add-row" *ngIf="_options.showAddNew">
      <nb-card-body>
        <form (submit)="$event.preventDefault(); addRow(nameInput.value); nameInput.value = ''"
              class="add-new-row-form">
          <div class="p-inputgroup">
            <input type="text" pInputText [placeholder]="_options.itemKey.toUpperCase()" #nameInput>
            <button type="submit" pButton pRipple label="Add new"></button>
          </div>
        </form>
      </nb-card-body>
    </nb-card>

    <ag-grid-angular
      style="width: 100%; height: 100%;"
      [modules]="modules"
      [rowData]="data$ | async"
      [columnDefs]="_colDefs"
      [defaultColDef]="defaultColDef"
      (cellValueChanged)="cellValueChanged($event)"

      (gridReady)="handleGridReady($event)"

      rowModelType="clientSide"
      [suppressCellSelection]="true"
      class="ag-theme-alpine"
      [pinnedBottomRowData]="_options.showAddNewAsTableRow === 'bottom' ? [newDataRow] : null"
      [pinnedTopRowData]="_options.showAddNewAsTableRow === 'top' ? [newDataRow] : null"
      [gridOptions]="gridOptions"
    >
    </ag-grid-angular>
  `,
  styleUrls: ['./simple-crud.component.scss'],
})
export class SimpleCrudComponent implements OnDestroy {
  public newDataRow = {};
  modules = AllModules;

  defaultColDef = {
    enableValue: true,
    sortable: true,
    filter: true,
    resizable: true,
  };

  private subscription = new Subscription();

  @Input() uniqName: string;

  // tslint:disable-next-line:variable-name
  _colDefs: ColDef[] = [];
  @Input() set colDefs(value: ColDef[]) {
    if (!value) {
      this._colDefs = [];
      return;
    }

    const self = this;
    value.forEach((row) => {
      row.filter = this._options.showFilter ? row.filter : false;
      row.floatingFilter = this._options.showFilter ? row.floatingFilter : false;
    });

    this._colDefs = [
      ...value,
      {
        headerName: 'Actions',
        filter: false,
        pinnedRowCellRenderer: BtnToolbarRenderer,
        pinnedRowCellRendererParams: {
          actions: (data: any) => [
            {
              onClick: () => {
                this.addRow(data);
                this.newDataRow = {};
              },
              title: 'Add row',
              icon: 'plus',
            },
          ],
        } as BtnToolbarRendererParams,
        cellRenderer: BtnToolbarRenderer,
        cellRendererParams: {
          actions: (data: any) => this._options.additionalActions(data, self.crudWrapper).concat([
            {
              onClick: () => { this.deleteRow(data); },
              title: 'Delete row',
              icon: 'trash',
            },
          ]),
        } as BtnToolbarRendererParams,
      },
    ];
  }

  private crudWrapper: CrudWrapper<any>;

  @Input() set collectionRef(collection: AngularFirestoreCollection<any>) {
    this.crudWrapper = new CrudWrapper(collection);
  }

  @Input() filter: (item: any) => boolean = () => true;

  @Input() gridOptions: GridOptions;

  // tslint:disable-next-line:variable-name
  _options: SimpleCrudOptions<any> = {
    showAddNew: true,
    showAddNewAsTableRow: null,
    showFilter: true,
    async beforeAdd() {
      return true;
    },
    async beforeUpdate() {
      return true;
    },
    async beforeDelete() {
      return true;
    },
    dataTransform(item) {
      return item;
    },
    itemKey: 'name',
    useKeyAsId: false,
    keyGenerator: (itemKeyValue => itemKeyValue),
    baseProperties: {},
    additionalActions() {
      return [];
    },
  };
  @Input() set options(optionsValue: SimpleCrudOptions<any>) {
    Object.assign(this._options, optionsValue);
    this.defaultColDef.filter = this._options.showFilter;
  }

  gridApi: GridApi;

  data$ = defer(() => {
    return this.crudWrapper.collectionRef.valueChanges({idField: 'id'}).pipe(map(
      (collection) => collection.map(this._options.dataTransform).filter(this.filter),
    ));
  });

  public ngOnDestroy() {
    this.subscription.unsubscribe();
  }

  public async updateRow(params: UpdateRowParams<any>): Promise<void> {
    if (await this._options.beforeUpdate(params)) {
      return this.crudWrapper.updateDoc(params);
    }
  }

  public async addRow(data: string | object) {
    if (data) {
      if (await this._options.beforeAdd(data)) {
        if (typeof data === 'string') {
          await this.crudWrapper.addDoc(Object.assign(this._options.baseProperties, {[this._options.itemKey]: data}),
            this._options.useKeyAsId ? this._options.keyGenerator(data) : null);
        }
        else {
          await this.crudWrapper.addDoc(Object.assign(this._options.baseProperties, data));
        }
      }
    }
  }

  public async deleteRow(row: { id: string, [key: string]: any }) {
    if (confirm('Are you sure you want to delete ' + (row[this._options.itemKey] || 'row'))) {
      if (await this._options.beforeDelete(row)) {
        await this.crudWrapper.deleteDoc(row.id);
      }
    }
  }

  handleGridReady(event: GridReadyEvent) {
    this.gridApi = event.api;
    event.api.sizeColumnsToFit();

    this.subscription.add(
      syncStateToLocalStorage(this.uniqName, event.columnApi, event.api),
    );
  }

  cellValueChanged(event: CellValueChangedEvent) {
    const data = event.data;

    if (event.oldValue === event.value) {
      return;
    }

    this.updateRow({
      oldValue: event.oldValue,
      value: event.value,
      field: event.colDef.field,
      row: data,
    });
  }

}
