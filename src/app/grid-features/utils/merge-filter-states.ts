import { forIn, merge, isEmpty } from 'lodash';

function cloneDeep<T>(val: T): T {
  return JSON.parse(JSON.stringify(val));
}

export function mergeFilterStates(state1: any, state2: any) {
  const s1 = state1 && cloneDeep(state1);
  const s2 = state2 && cloneDeep(state2);
  if (!isEmpty(s1) && !isEmpty(s2)) {
    forIn(s2, (value: any, key: string) => {
      if (
        s1[key] &&
        !s1.condition1 &&
        !value.condition1 &&
        value.filterType !== 'set' && s1[key]?.filterType !== 'set'
      ) {
        s1[key] = {
          condition1: value,
          condition2: s1[key],
          operator: 'AND',
          filterType: value.filterType,
        };
      } else if (value.filterType === 'set' && s1[key]?.filterType === 'set') {
        s1[key].values = [...s1[key].values, ...value.values];
      }
    });
    return merge(s2, s1);
  } else if (isEmpty(s1) && s2) {
    return s2;
  } else if (isEmpty(s2) && s1) {
    return s1;
  } else {
    return [];
  }
}
