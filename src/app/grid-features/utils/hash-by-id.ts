export const hashById = <T extends { id: string }>(data: T[]) => {
  return data.reduce<{ [id: string]: T }>((acc, item) => {
    acc[item.id] = item;
    return acc;
  }, {});
};
