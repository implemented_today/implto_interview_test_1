import { ColumnApi, GridApi } from '@ag-grid-community/core';
import { fromEvent, merge, Subscription } from 'rxjs';
import { take, debounceTime } from 'rxjs/operators';

export function syncStateToLocalStorage(uniqName: string, columnApi: ColumnApi, gridApi: GridApi) {
  const subscription = new Subscription();

  const stateChangeEvents = [
    'sortChanged',
    // 'filterChanged',
    // 'columnEverythingChanged',
    'columnVisible',
    'columnResized',
    'columnPinned',
    'columnMoved',
    'columnGroupOpened',
    'columnPivotChanged',
  ];

  const key = 'gridState_' + uniqName;

  const storedState = localStorage.getItem(key);

  if (storedState) {
    subscription.add(fromEvent(gridApi, 'gridColumnsChanged').pipe(
      take(1),
    ).subscribe(() => {
      const state = JSON.parse(storedState);
      columnApi.setColumnState(state.colState);
      columnApi.setColumnGroupState(state.groupState);
      // gridApi.setFilterModel(state.filterState);
    }));
  }

  subscription.add(merge(...stateChangeEvents.map((eventName) => fromEvent(gridApi, eventName)))
    .pipe(
      debounceTime(300),
    )
    .subscribe(() => {
      const newState = {
        colState: columnApi.getColumnState(),
        groupState: columnApi.getColumnGroupState(),
        // filterState: gridApi.getFilterModel(),
      };

      localStorage.setItem(key, JSON.stringify(newState));
    }));


  return subscription;
}
