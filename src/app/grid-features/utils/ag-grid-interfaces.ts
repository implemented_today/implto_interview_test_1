import { RowNode, VirtualRowRemovedEvent } from '@ag-grid-enterprise/all-modules';
import { ColumnApi, GridApi, GridOptions } from '@ag-grid-community/core';
import { GetDetailRowData } from '@ag-grid-community/core/dist/es6/interfaces/masterDetail';

export interface IDetailCellRendererParamsGetterParams {
  node: RowNode;
  api: GridApi;
  columnApi: ColumnApi;
  addRenderedRowListener: (eventName: 'virtualRowRemoved', cb: (e: VirtualRowRemovedEvent) => void) => void;
}

export interface IDetailCellRendererParams {
  detailGridOptions: GridOptions;
  getDetailRowData: GetDetailRowData;
  refreshStrategy?: 'rows' | 'everything' | 'nothing';
  template?: string | ((params: any) => string);
}

export type IDetailCellRendererParamsGetter = (params: IDetailCellRendererParamsGetterParams) => IDetailCellRendererParams;
