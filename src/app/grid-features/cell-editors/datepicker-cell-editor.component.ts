import { Component, ViewChild, AfterViewInit, NgModule } from '@angular/core';
import { MatDatepicker, MatDatepickerInputEvent, MatDatepickerModule } from '@angular/material/datepicker';
import { UniversalEditorComponent, UniversalCellEditorParams } from './universal-cell-editor-params';
import { CommonModule } from '@angular/common';
import { MatNativeDateModule } from '@angular/material/core';
import { DateTime } from 'luxon';

export interface DatepickerCellEditorParams {
  suppressAutoOpen?: boolean;
}

@Component({
  selector: 'kt-ag-grid-material-datepicker-editor',
  template: `
    <input
      [matDatepicker]="picker"
      (dateInput)="handleDateInput($event)"
      (dateChange)="handleDateChange($event)"
      [value]="value"/>
    <mat-datepicker-toggle [for]="picker"></mat-datepicker-toggle>
    <mat-datepicker
      panelClass="ag-custom-component-popup"
      #picker
    ></mat-datepicker>
  `,
})
export class DatepickerCellEditorComponent implements UniversalEditorComponent, AfterViewInit {
  params: DatepickerCellEditorParams & UniversalCellEditorParams;
  value: Date;
  valueAsIso: string;

  @ViewChild('picker', { read: MatDatepicker }) picker: MatDatepicker<Date>;

  ngAfterViewInit() {
    if (!this.params.suppressAutoOpen) {
      Promise.resolve().then(() => {
        this.picker.open();
      });
    }
  }

  handleDateInput(event: MatDatepickerInputEvent<Date>) {
    this.setDate(event);
  }

  private setDate(event: MatDatepickerInputEvent<Date>) {
    if (event.value) {
      // use this "long" expression instead of fromJsDate
      // to overcome issue with -1 day because of local tz -> UTC conversion
      this.valueAsIso = DateTime.utc(event.value.getFullYear(), event.value.getMonth() + 1, event.value.getDate()).toISODate();
    } else {
      this.valueAsIso = null;
    }
  }

  handleDateChange(event: MatDatepickerInputEvent<Date>) {
    this.setDate(event);
    this.params.stopEditing();
  }

  agInit(params: DatepickerCellEditorParams & UniversalCellEditorParams): void {
    this.params = params;
    this.value = params.value ? new Date(params.value) : null;
    this.valueAsIso = params.value;
  }

  getValue(): string {
    return this.valueAsIso;
  }
}

@NgModule({
  declarations: [
    DatepickerCellEditorComponent,
  ],
  imports: [
    CommonModule,
    MatDatepickerModule,
    MatNativeDateModule,
  ],
  exports: [],
})
export class DatepickerCellEditorModule {}
