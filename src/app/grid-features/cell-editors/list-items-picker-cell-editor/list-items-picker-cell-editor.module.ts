import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { ListItemsPickerCellEditorComponent } from './list-items-picker-cell-editor.component';
import { MultiSelectModule } from 'primeng/multiselect';
import { FormsModule } from '@angular/forms';
import { DropdownModule } from 'primeng/dropdown';
import { MultiselectWithOrderModule } from '../../../components/multiselect-with-order/multiselect-with-order';

@NgModule({
  declarations: [ListItemsPickerCellEditorComponent],
  imports: [
    CommonModule,
    MultiSelectModule,
    FormsModule,
    DropdownModule,
    MultiselectWithOrderModule,
  ],
})
export class ListItemsPickerCellEditorModule {}
