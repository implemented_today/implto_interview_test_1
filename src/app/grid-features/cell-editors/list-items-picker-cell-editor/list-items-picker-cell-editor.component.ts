import { AfterViewInit, Component, ViewChild } from '@angular/core';
import { MultiSelect } from 'primeng/multiselect';
import { Dropdown } from 'primeng/dropdown';
import { UniversalCellEditorParams, UniversalEditorComponent } from '../universal-cell-editor-params';
import { ColDef, ISetFilterParams } from '@ag-grid-community/core';
import { MultiselectWithOrder } from '../../../components/multiselect-with-order/multiselect-with-order';

export interface Option {
  label: string;
  value: any;
  disabled?: boolean;
}

function getLabelByEntityId(options: Option[], id: string): string {
  const el = options.find((option) => option.value === id);
  return el ? el.label : null;
}

export interface ListItemsPickerCellEditorParams {
  options: Option[] | (() => Option[]);
  multi?: boolean;
  sortable?: boolean;
  selectionLimit?: number;
  onChange?: (value: string | string[]) => string | string[];
  suppressAutoOpen?: boolean;
  cellEditorParamsFunc?: (params: any) => Partial<ListItemsPickerCellEditorParams>,
}

function normalizeOptions(options: Option[] | (() => Option[])) {
  if (typeof options === 'function') {
    return options();
  }

  return options;
}

@Component({
  selector: 'app-list-items-picker-cell-editor',
  template: `
    <p-multiSelect
      *ngIf="params.multi && !params.sortable"
      [options]="getOptions()"
      appendTo="body"
      [ngModel]="value"
      [selectionLimit]="params.selectionLimit"
      (ngModelChange)="_handleValueChange($event)"
      (onPanelHide)="params.stopEditing();"
      #dropdown></p-multiSelect>

    <p-dropdown
      *ngIf="!params.multi"
      [options]="getOptions()"
      appendTo="body"
      [filter]="true"
      [ngModel]="value"
      (ngModelChange)="_handleValueChange($event)"
      (onHide)="params.stopEditing();"
      placeholder="Select value"
      [showClear]="true"
      #dropdown>
    </p-dropdown>

    <p-multiSelectWithOrder
      *ngIf="params.multi && params.sortable"
      [options]="getOptions()"
      appendTo="body"
      [ngModel]="value"
      [selectionLimit]="params.selectionLimit"
      (ngModelChange)="_handleValueChange($event)"
      (onPanelHide)="params.stopEditing();"
      #dropdown></p-multiSelectWithOrder>

  `,
  styleUrls: ['./list-items-picker-cell-editor.component.scss'],
})
export class ListItemsPickerCellEditorComponent implements UniversalEditorComponent, AfterViewInit {
  params: UniversalCellEditorParams & ListItemsPickerCellEditorParams;
  value: string | string[];

  @ViewChild('dropdown', {static: false}) dropdown: Dropdown | MultiSelect | MultiselectWithOrder;

  static init(params: ListItemsPickerCellEditorParams): ColDef {
    return {
      cellEditorFramework: ListItemsPickerCellEditorComponent,
      cellEditorParams: params.cellEditorParamsFunc || params,
      filter: 'agSetColumnFilter',

      // https://www.ag-grid.com/javascript-grid-filter-set/#set-filter-parameters
      filterParams: (): Partial<ISetFilterParams> => {
        const options = normalizeOptions(params.options);

        return {
          // newRowsAction: 'keep',
          values: [null, ...options.map((o) => o.value)],
          valueFormatter: ({value}) => {
            return getLabelByEntityId(options, value);
          },
        };
      },

      valueFormatter: (formatterParams) => {
        let value = formatterParams.value;
        const options = normalizeOptions(params.cellEditorParamsFunc ?
          params.cellEditorParamsFunc(formatterParams).options : params.options);

        if (Array.isArray(value) && params.multi) {
          value = value.filter((el) => options.find((option) => option.value === el));
          if (value.length > 2) {
            return `${value.length} selected`;
          }

          return value.map((val: string) => getLabelByEntityId(options, val)) as any;
        }

        const label = getLabelByEntityId(options, value);

        return label ?? (!params.multi && value ? '' : label);
      },
    };
  }

  public ngAfterViewInit() {
    if (!this.params.suppressAutoOpen) {
      Promise.resolve().then(() => {
        this.dropdown.show();
      });
    }
  }

  isPopup(): boolean {
    return false;
  }

  isCancelBeforeStart(): boolean {
    return false;
  }

  isCancelAfterEnd(): boolean {
    return false;
  }

  getOptions() {
    return normalizeOptions(this.params.options);
  }

  agInit(params: UniversalCellEditorParams & ListItemsPickerCellEditorParams): void {
    this.params = params;
    this.value = params.value;
  }

  getValue(): string | string[] {
    return this.value;
  }

  _handleValueChange(value: string | string[]) {
    this.value = this.params.onChange ? this.params.onChange(value) : value;
    if (this.value) {
      const optionsValues = normalizeOptions(this.params.options).map((option) => option.value);
      if (this.value instanceof Array) {
        this.value = this.value.filter((item) => optionsValues.includes(item));
      } else if (!optionsValues.includes(this.value as string)) {
        this.value = null;
      }
    }
  }
}
