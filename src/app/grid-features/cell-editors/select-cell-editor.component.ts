import { Component, AfterViewInit, ViewChild, NgModule } from '@angular/core';
import { MatSelect, MatSelectModule } from '@angular/material/select';
import { CommonModule } from '@angular/common';
import { UniversalEditorComponent, UniversalCellEditorParams } from './universal-cell-editor-params';

export interface SelectCellEditorOption {
  value: string;
  label: string;
}

export interface SelectCellEditorParams {
  options: SelectCellEditorOption[];
  suppressAutoOpen?: boolean;
}

@Component({
  selector: 'ng-rich-select-cell-editor',
  template: `
    <mat-select
      #control
      [value]="value == undefined ? '' : value"
      (selectionChange)="handleChange($event.value)"
    >
      <mat-option *ngFor="let option of params.options" [value]="option.value">
        {{option.label}}
      </mat-option>
    </mat-select>
  `,
})
export class SelectCellEditorComponent implements UniversalEditorComponent, AfterViewInit {
  @ViewChild(MatSelect, { static: true }) public control: MatSelect;

  value: string | number | boolean;
  options: { label: string, value: string }[];
  params: UniversalCellEditorParams & SelectCellEditorParams;

  public agInit(params: UniversalCellEditorParams & SelectCellEditorParams): void {
    this.value = params.value;
    this.params = params;
  }

  public ngAfterViewInit() {
    if (!this.params.suppressAutoOpen) {
      Promise.resolve().then(() => {
        this.control.open();
      });
    }
  }

  handleChange(value: string | number | boolean) {
    this.value = value;
    this.params.stopEditing();
  }

  public getValue(): string | number | boolean {
    return this.value;
  }
}

@NgModule({
  declarations: [SelectCellEditorComponent],
  imports: [
    CommonModule,
    MatSelectModule,
  ],
})
export class SelectCellEditorModule {}
