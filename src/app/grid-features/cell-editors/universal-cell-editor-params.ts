import { ColDef, ICellEditor } from '@ag-grid-community/core';
import { AgFrameworkComponent } from '@ag-grid-community/angular/lib/interfaces';

/**
 * Cell Editors usually meant to be used only in AgGrid but in this app the also used in angular components.
 * This reason of that is to eliminate any differences in editing in ag-grid and rest of application.
 *
 * We use absolutely the same components, with the same getter/setters and behaviour around of app.
 *
 * When component is called from Angular environment it tries to mimic interface of AgGrid.
 * But it is not possible to create all object structures to fully mimic grid's API.
 *
 * This interface is subset of properties which is safe to use in cell editors.
 * Properties from this interface will be populated in both AgGrid and Angular component usages.
 */
export interface UniversalCellEditorParams {
  value: any;
  colDef: ColDef;
  stopEditing: () => void;

  /**
   * not available in Angular context
   */
  charPress?: string;
}

export interface UniversalEditorComponent extends ICellEditor, AgFrameworkComponent<UniversalCellEditorParams> {}
