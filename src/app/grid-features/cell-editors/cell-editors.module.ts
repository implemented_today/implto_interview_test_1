import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { DatepickerCellEditorModule } from './datepicker-cell-editor.component';
import { NumericCellEditorComponent } from './numeric-cell-editor.component';
import { MatDatepickerModule } from '@angular/material/datepicker';
import { SelectCellEditorModule } from './select-cell-editor.component';
import { SimpleTextCellEditorComponent } from './simple-text-cell-editor.component';

@NgModule({
  declarations: [
    NumericCellEditorComponent,
    SimpleTextCellEditorComponent,
  ],
  imports: [
    CommonModule,
    MatDatepickerModule,
    SelectCellEditorModule,
    DatepickerCellEditorModule,
  ],
  exports: [],
})
export class CellEditorsModule { }
