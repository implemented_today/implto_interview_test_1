import { AfterViewInit, Component, NgZone, ViewChild, ElementRef } from '@angular/core';
import { UniversalEditorComponent, UniversalCellEditorParams } from './universal-cell-editor-params';
import { ColDef } from '@ag-grid-community/core';

/**
 * return or number or null if empty
 */
function normalizeValue(value: string | number) {
  if (value === undefined || value === null) {
    return null;
  }

  return Number(value);
}

export interface NumericCellEditorParams {
  getValue?: (value: number) => number;
  setValue?: (value: number) => number;
}

@Component({
  selector: 'kt-numeric-cell',
  template: `
    <input #input type="number" class="ag-input-field-input custom-cell-editor"
           (blur)="params.stopEditing()"
           [value]="value"
           (input)="handleChange(input.value)"/>
  `,
  styles: [`
    /* Chrome, Safari, Edge, Opera */
    input::-webkit-outer-spin-button,
    input::-webkit-inner-spin-button {
      -webkit-appearance: none;
      margin: 0;
    }

    /* Firefox */
    input[type=number] {
      -moz-appearance: textfield;
    }
  `],
})
export class NumericCellEditorComponent implements UniversalEditorComponent, AfterViewInit {
  static init(): ColDef {
    return {
      cellEditorFramework: NumericCellEditorComponent,
    };
  }

  static initAsPercentageEditor(): ColDef {
    return {
      cellEditorFramework: NumericCellEditorComponent,
      cellEditorParams: {
        getValue: (value: number) => {
          // 0.55 * 100 give 55.0000001 result in javascript,
          // fixing this by round to two decimals
          return Math.round(((value * 100) + Number.EPSILON) * 100) / 100;
        },
        setValue: (value: number) => {
          return value / 100;
        },
      } as NumericCellEditorParams,
    };
  }

  public params: NumericCellEditorParams & UniversalCellEditorParams;
  public value: string;
  public oldValue: number;

  private isTouched: boolean;

  @ViewChild('input', { static: true }) public input: ElementRef<HTMLInputElement>;

  constructor(private zone: NgZone) {}

  agInit(params: NumericCellEditorParams & UniversalCellEditorParams): void {
    this.params = params;

    this.oldValue = params.value;

    const value = normalizeValue(params.value);
    this.value = value === null ? '' : (params.getValue ? params.getValue(value) : value).toString();
  }

  handleChange(value: string) {
    this.isTouched = true;
    this.value = value;
  }

  getValue(): number {
    if (!this.isTouched) {
      return this.oldValue;
    }

    if (this.value === '') {
      return null;
    }

    const value = Number(this.value);
    return this.params.setValue ? this.params.setValue(value) : value;
  }

  isCancelBeforeStart(): boolean {
    // only start edit if key pressed is a number, not a letter
    return this.params.charPress && !'1234567890'.indexOf(this.params.charPress);
  }

  isCancelAfterEnd(): boolean {
    return false;
  }

  ngAfterViewInit() {
    this.zone.runOutsideAngular(() => {
      setTimeout(() => {
        this.input.nativeElement.focus();
        this.input.nativeElement.select();
      });
    });
  }
}
