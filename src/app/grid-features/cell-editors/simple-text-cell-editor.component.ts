import { Component, AfterViewInit, ViewChild, ElementRef } from '@angular/core';
import { UniversalEditorComponent, UniversalCellEditorParams } from './universal-cell-editor-params';

@Component({
  selector: 'simple-text-input',
  template: `
    <input #control
           (blur)="params.stopEditing()"
           [value]="value == undefined ? '' : value"
           (input)="value = control.value"/>
  `,
})
export class SimpleTextCellEditorComponent implements UniversalEditorComponent, AfterViewInit {
  @ViewChild('control', { static: true }) public control: ElementRef<HTMLInputElement>;

  value: string;
  options: { label: string, value: string }[];
  params: UniversalCellEditorParams;

  public agInit(params: UniversalCellEditorParams): void {
    this.value = params.value;
    this.params = params;
  }

  public ngAfterViewInit() {
    const control = this.control.nativeElement;

    Promise.resolve().then(() => {
      control.focus();
      control.select();
    });
  }

  public getValue(): string {
    return this.value;
  }
}
