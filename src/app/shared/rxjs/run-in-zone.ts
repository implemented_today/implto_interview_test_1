import { Observable, OperatorFunction } from 'rxjs';
import { NgZone } from '@angular/core';

export function runInZone<T>(zone: NgZone): OperatorFunction<T, T> {
  return (source) => {
    return new Observable((observer) => {
      const onNext = (value: T) => zone.run(() => observer.next(value));
      const onError = (e: any) => zone.run(() => observer.error(e));
      const onComplete = () => zone.run(() => observer.complete());
      return source.subscribe(onNext, onError, onComplete);
    });
  };
}

export function runOutOfZone<T>(zone: NgZone): OperatorFunction<T, T> {
  return (source) => {
    return new Observable((observer) => {
      const onNext = (value: T) => zone.runOutsideAngular(() => observer.next(value));
      const onError = (e: any) => zone.runOutsideAngular(() => observer.error(e));
      const onComplete = () => zone.runOutsideAngular(() => observer.complete());
      return source.subscribe(onNext, onError, onComplete);
    });
  };
}
