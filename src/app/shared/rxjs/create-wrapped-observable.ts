import { ObservedValueOf, Observable, defer, of, SubscribableOrPromise } from 'rxjs';
import { map, startWith, catchError } from 'rxjs/operators';

export interface WrappedObservableResult<T> {
  isLoading: boolean;
  noItems: boolean;
  items: T;
  error?: boolean;
}

export type WrappedObservable<T> = Observable<WrappedObservableResult<T>>;

export function createWrappedObservable<O extends SubscribableOrPromise<any[]>>(
  observableFactory: () => O,
): WrappedObservable<ObservedValueOf<O>> {
  return defer(observableFactory).pipe(
    map((items) => ({
      items,
      isLoading: false,
      noItems: items.length === 0,
    })),
    startWith({ isLoading: true, items: null, noItems: false }),
    catchError((e) => {
      console.error(e);

      return of({
        items: null,
        isLoading: false,
        noItems: true,
        error: true,
      });
    }),
  );
}
