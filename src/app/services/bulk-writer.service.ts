import { Observable } from 'rxjs';
import makeChunks from 'lodash/chunk';
import firebase from 'firebase/compat/app';

export type UpdateProgress = {
  total: number,
  done: number,
  percentage: number,
};

export class FeBulkWriter {
  private MAX_FIRESTORE_BATCH_SIZE = 500;

  private operations: Array<(batch: firebase.firestore.WriteBatch) => void> = [];

  constructor(private firestore: firebase.firestore.Firestore) { }

  /**
   * Provide callback which will add operation to batch.
   * Warning only one operation is in one call is allowed!
   */
  operation(op: (batch: firebase.firestore.WriteBatch) => void): this {
    this.operations.push(op);
    return this;
  }

  commit(): Observable<UpdateProgress> {
    const chunks = makeChunks(this.operations, this.MAX_FIRESTORE_BATCH_SIZE);

    const getProgress = (current: number): UpdateProgress => {
      const done = chunks.slice(0, current).reduce((sum, chunk) => sum + chunk.length, 0);
      const total = this.operations.length;

      return {
        done,
        total,
        percentage: (done / total) * 100,
      };
    };

    return new Observable<UpdateProgress>((subscriber) => {
      const doBatch = async () => {
        subscriber.next(getProgress(0));
        let current = 1;

        for (const chunk of chunks) {
          const batch = this.firestore.batch();
          chunk.forEach((operation) => operation(batch));

          await batch
            .commit()
            .catch((e) => {
              console.error(e);
            })
            .finally(() => {
              subscriber.next(getProgress(current));
            });

          current++;
        }
      };

      doBatch().finally(() => setTimeout(() => subscriber.complete(), 1000));
    });
  }
}
