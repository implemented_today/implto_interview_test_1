import { Injectable } from '@angular/core';
import { NbToastrService } from '@nebular/theme';

@Injectable({providedIn: 'root'})
export class NotificationService {
  constructor(
    private toastrService: NbToastrService,
  ) {}

  showError(e: string, duration = 7500) {
    console.error(e);
    this.toastrService.warning(e, null, {
      duration,
    });

  }

  showMessage(message: string, duration = 7500) {
    this.toastrService.info(message, null, {
      duration,
    });
  }
}
