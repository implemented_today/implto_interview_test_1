import { Injectable } from '@angular/core';
import { UploadImageRequest, UploadImageResponse } from '@shared/upload-image';
import { take } from 'rxjs/operators';
import { AngularFireFunctions } from '@angular/fire/compat/functions';
import { AngularFireStorage } from '@angular/fire/compat/storage';

@Injectable({providedIn: 'root'})
export class FileUploadService {
  constructor(
    private functions: AngularFireFunctions,
    private storage: AngularFireStorage,
  ) {
  }

  public async uploadFileToStorage(filePath: string, fileData: Blob): Promise<string> {
    const ref = this.storage.ref(filePath);
    await ref.put(fileData);
    return ref.getDownloadURL().toPromise();
  }

  public downloadFileFromStorage(filePath: string): Promise<string> {
    const ref = this.storage.ref(filePath);
    try {
      return ref.getDownloadURL().pipe(take(1)).toPromise();
    } catch (e) {
      throw e;
    }
  }

  public async removeFileFromStorage(filePath: string): Promise<void> {
    const ref = this.storage.ref(filePath);
    try {
      await ref.delete().toPromise();
    }
    catch (e) {
      if (e.code !== 'storage/object-not-found') {
        throw e;
      }
    }
  }

  public async resizeAndUploadImage(uploadImageRequestData: UploadImageRequest): Promise<UploadImageResponse> {
    return this.functions.httpsCallable('uploadImage')(uploadImageRequestData).toPromise();
  }
}
