import firebaseConfig from './firebase-configs/firebase-config.dev.json';

export const environment = {
  production: false,
  emulator: true,
  firebase: firebaseConfig
};
