import firebaseConfig from './firebase-configs/firebase-config.dev.json';

export const environment = {
  production: false,
  emulator: false,
  firebase: firebaseConfig
};
