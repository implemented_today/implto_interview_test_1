import firebaseConfig from './firebase-configs/firebase-config.prod.json';

export const environment = {
  production: true,
  emulator: false,
  firebase: firebaseConfig
};
