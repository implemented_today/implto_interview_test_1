## Development server

Run `ng serve` for a dev server. Navigate to `http://localhost:4200/`. The app will automatically reload if you change any of the source files.

## Build

Run `yarn build:dev` to build the project. The build artifacts will be stored in the `dist/` directory.

## Running unit tests

Run `yarn test` to execute the unit tests.

## Running end-to-end tests

Run `yarn e2e` to execute the end-to-end tests via [Protractor](http://www.protractortest.org/).

# Cloud Functions

Cloud functions is stored in `./functions` folder and written in Typescript.

## Local Development of cloud function

### Install dependencies:

Install [Google Cloud SDK](https://cloud.google.com/sdk/docs/install)

Install NPM dependencies:

```bash
cd ./functions
yarn install
```
### Import Database Data from cloud

(script is written in bash and will not work on Windows natively, please use [WSL](https://docs.microsoft.com/en-us/windows/wsl/install-win10))

```bash
yarn db:update
```

This will call script which export and download data from Cloud Firestore to local machine.

You should have Cloud Firestore roles: Owner, Cloud Datastore Owner,
or Cloud Datastore Import Export Admin to perform this operation.

TODO: might need to state here bucket roles as well.

### Serve functions with hot reload.

You need to run Typescript in watch mode and firestore emulator.
You can do this by running both commands in one terminal using `&` but
it leads to really messed output from both of commands so recommended way
is to jest run two terminal windows with two commands:

```bash
cd ./functions

yarn build:watch

# then in separate terminal window
yarn serve
```

The functions will be re-compiled and updated while you develop.

### Adding new function

Folder structure:

```
├── functions
│  └── src
│      ├── index.ts
│      ├── function1
│      │    ├── index.ts
│      │    └── some-other-source.ts
│      ├── function2.ts
```

To add new function create a folder with function name and create `index.ts` file.
Export your function with desirable name from root `index.ts` file.

If you have simple on-file function is's allowed to store it in `./src` folder without
subfolder to avoid boilerplate.


### Debugging functions with breakpoints

```
npx firebase emulators:start --only functions --inspect-functions

or 

yarn serve --inspect-functions

```

This will start listening on debugger connection on default 9229 port.
Then you can connect with Chrome Developer Tools (take a look at this [extension](https://chrome.google.com/webstore/detail/nodejs-v8-inspector-manag/gnhhdgbaldcilmgcpfddgdbkhjohddkj)) or you favourite IDE


### Testing functions

There are 2 type of tests:

1. Unit tests which testing only logic and not depend on the firestore
2. Integration tests which need emulator run to execute.

They currently have no differences in naming conventions and would be executed by jest together.
But during developing new tests you might want not to start emulator if you develop completely offline test

```
cd ./functions

// NOTE: we launch emulator with --only firestore flag because we don't want emulator to serve our cloud functions
firebase emulators:start --only firestore

// run in separate therminal
jest
```

Or you can run/debug test directly from your IDE (Webstorm support it out of the box by clicking on a green icon on the line)
and develop tests using breakpoints.

Just don't forget to start emulator before.

#### Running test on CI
```
cd ./functions
yarn test:ci
```

This would launch emulator execute tests and shutdown emulator when test suite is complete.

#### Tests troubleshooting
Some times jest use stale files from cache. To clear cache just run command  jest --clearCache` from functions directory.

### Useful commands

Sometimes emulator not able to gracefully stop. It lefts zombie processes which keep ports
opened and blocking new emulator instance to start.

use following commands to fix this:

```bash
# show all processes which take port 8080
sudo lsof -i tcp:8080

# kill zombie process by pid
sudo kill <process pid>
```
